import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { tap } from 'rxjs/operators';
import 'rxjs/add/observable/of';

@Injectable()
export class ReplaceGuard implements CanActivate {

  constructor(private location: Location,
    private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const { i, ...queryParams } = next.queryParams;
    next.queryParams = queryParams;
    if (i) {
      console.log('i');
      return Observable.of(true)
        .pipe(
          tap(() => {
            setTimeout(() => {
              const url = this.router.serializeUrl(this.router.createUrlTree([], { queryParams }));
              this.location.replaceState(url);
            }, 1)
          })
        )
    }
    return Observable.of(true);
  }
}
