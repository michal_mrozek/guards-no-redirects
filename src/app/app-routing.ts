import { Routes } from "@angular/router";
import { FirstComponent } from "./first/first.component";
import { ReplaceGuard } from "./replace.guard";
import { AppComponent } from "./app.component";
import { NextReplaceGuard } from "./next-replace.guard";

export const routes: Routes = [
    {
        path: '',
        component: AppComponent,
        canActivate: [ReplaceGuard, NextReplaceGuard],
        children: [
            {
                path: 'first',
                component: FirstComponent
            },
            {
                path: '',
                redirectTo: 'first',
                pathMatch: 'full'
            },
            {
                path: '**',
                redirectTo: 'first'
            }
        ]
    }
]