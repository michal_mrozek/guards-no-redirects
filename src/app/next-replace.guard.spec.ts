import { TestBed, async, inject } from '@angular/core/testing';

import { NextReplaceGuard } from './next-replace.guard';

describe('NextReplaceGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NextReplaceGuard]
    });
  });

  it('should ...', inject([NextReplaceGuard], (guard: NextReplaceGuard) => {
    expect(guard).toBeTruthy();
  }));
});
