import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FirstComponent } from './first/first.component';
import { ReplaceGuard } from './replace.guard';
import { RouterModule } from '@angular/router';
import { routes } from './app-routing';
import { AppBootstrapComponent } from './app-bootstrap/app-bootstrap.component';
import { NextReplaceGuard } from './next-replace.guard';


@NgModule({
  declarations: [
    AppComponent,
    FirstComponent,
    AppBootstrapComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    ReplaceGuard,
    NextReplaceGuard
  ],
  bootstrap: [AppBootstrapComponent]
})
export class AppModule { }
