import { TestBed, async, inject } from '@angular/core/testing';

import { ReplaceGuard } from './replace.guard';

describe('ReplaceGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReplaceGuard]
    });
  });

  it('should ...', inject([ReplaceGuard], (guard: ReplaceGuard) => {
    expect(guard).toBeTruthy();
  }));
});
